use core::convert::Infallible;

use embedded_hal::digital::v2::InputPin;

use rp2040_hal::gpio::pin::{bank0, Pin, PullUpInput};

#[derive(Clone, Copy)]
#[repr(u8)]
pub enum Switch {
    Sw0,
    Sw1,
    Sw2,
    Sw3,
    Sw4,
    Sw5,
    Sw6,
    Sw7,
    Sw8,
    Sw9,
    Sw10,
    Sw11,
    Sw12,
    Sw13,
    Sw14,
    Sw15,
}

impl Switch {
    pub fn coords(&self) -> (u8, u8) {
        match self {
            Self::Sw0 => (0, 0),
            Self::Sw1 => (1, 0),
            Self::Sw2 => (2, 0),
            Self::Sw3 => (3, 0),
            Self::Sw4 => (0, 1),
            Self::Sw5 => (1, 1),
            Self::Sw6 => (2, 1),
            Self::Sw7 => (3, 1),
            Self::Sw8 => (0, 2),
            Self::Sw9 => (1, 2),
            Self::Sw10 => (2, 2),
            Self::Sw11 => (3, 2),
            Self::Sw12 => (0, 3),
            Self::Sw13 => (1, 3),
            Self::Sw14 => (2, 3),
            Self::Sw15 => (3, 3),
        }
    }

    pub fn try_from_coords(x: u8, y: u8) -> Option<Self> {
        match (x, y) {
            (0, 0) => Some(Self::Sw0),
            (1, 0) => Some(Self::Sw1),
            (2, 0) => Some(Self::Sw2),
            (3, 0) => Some(Self::Sw3),
            (0, 1) => Some(Self::Sw4),
            (1, 1) => Some(Self::Sw5),
            (2, 1) => Some(Self::Sw6),
            (3, 1) => Some(Self::Sw7),
            (0, 2) => Some(Self::Sw8),
            (1, 2) => Some(Self::Sw9),
            (2, 2) => Some(Self::Sw10),
            (3, 2) => Some(Self::Sw11),
            (0, 3) => Some(Self::Sw12),
            (1, 3) => Some(Self::Sw13),
            (2, 3) => Some(Self::Sw14),
            (3, 3) => Some(Self::Sw15),
            _ => None,
        }
    }
}

pub const ALL_SWITCHES: [Switch; 16] = [
    Switch::Sw0,
    Switch::Sw1,
    Switch::Sw2,
    Switch::Sw3,
    Switch::Sw4,
    Switch::Sw5,
    Switch::Sw6,
    Switch::Sw7,
    Switch::Sw8,
    Switch::Sw9,
    Switch::Sw10,
    Switch::Sw11,
    Switch::Sw12,
    Switch::Sw13,
    Switch::Sw14,
    Switch::Sw15,
];

// TODO: probably want debouncing

pub struct Keypad {
    pub sw0: Pin<bank0::Gpio21, PullUpInput>,
    pub sw1: Pin<bank0::Gpio20, PullUpInput>,
    pub sw2: Pin<bank0::Gpio19, PullUpInput>,
    pub sw3: Pin<bank0::Gpio18, PullUpInput>,
    pub sw4: Pin<bank0::Gpio17, PullUpInput>,
    pub sw5: Pin<bank0::Gpio16, PullUpInput>,
    pub sw6: Pin<bank0::Gpio15, PullUpInput>,
    pub sw7: Pin<bank0::Gpio14, PullUpInput>,
    pub sw8: Pin<bank0::Gpio13, PullUpInput>,
    pub sw9: Pin<bank0::Gpio12, PullUpInput>,
    pub sw10: Pin<bank0::Gpio11, PullUpInput>,
    pub sw11: Pin<bank0::Gpio10, PullUpInput>,
    pub sw12: Pin<bank0::Gpio9, PullUpInput>,
    pub sw13: Pin<bank0::Gpio8, PullUpInput>,
    pub sw14: Pin<bank0::Gpio7, PullUpInput>,
    pub sw15: Pin<bank0::Gpio6, PullUpInput>,
}

impl Keypad {
    pub fn switch(&self, switch: Switch) -> &dyn InputPin<Error = Infallible> {
        match switch {
            Switch::Sw0 => &self.sw0,
            Switch::Sw1 => &self.sw1,
            Switch::Sw2 => &self.sw2,
            Switch::Sw3 => &self.sw3,
            Switch::Sw4 => &self.sw4,
            Switch::Sw5 => &self.sw5,
            Switch::Sw6 => &self.sw6,
            Switch::Sw7 => &self.sw7,
            Switch::Sw8 => &self.sw8,
            Switch::Sw9 => &self.sw9,
            Switch::Sw10 => &self.sw10,
            Switch::Sw11 => &self.sw11,
            Switch::Sw12 => &self.sw12,
            Switch::Sw13 => &self.sw13,
            Switch::Sw14 => &self.sw14,
            Switch::Sw15 => &self.sw15,
        }
    }

    pub fn is_pressed(&self, switch: Switch) -> bool {
        self.switch(switch).is_low().unwrap()
    }
}

#[macro_export]
macro_rules! make_keypad {
    ($pins:expr) => {
        $crate::input::Keypad {
            sw0: $pins.gpio21.into_mode(),
            sw1: $pins.gpio20.into_mode(),
            sw2: $pins.gpio19.into_mode(),
            sw3: $pins.gpio18.into_mode(),
            sw4: $pins.gpio17.into_mode(),
            sw5: $pins.gpio16.into_mode(),
            sw6: $pins.gpio15.into_mode(),
            sw7: $pins.gpio14.into_mode(),
            sw8: $pins.gpio13.into_mode(),
            sw9: $pins.gpio12.into_mode(),
            sw10: $pins.gpio11.into_mode(),
            sw11: $pins.gpio10.into_mode(),
            sw12: $pins.gpio9.into_mode(),
            sw13: $pins.gpio8.into_mode(),
            sw14: $pins.gpio7.into_mode(),
            sw15: $pins.gpio6.into_mode(),
        }
    };
}
