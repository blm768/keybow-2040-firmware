use rgb::RGB8;

use usbd_human_interface_device::page::Keyboard;

use crate::input::{Keypad, Switch, ALL_SWITCHES};

pub trait Keymap {
    type Keys: IntoIterator<Item = Keyboard>;
    fn keys(&self, switch: Switch) -> Self::Keys;
    fn color(&self, _switch: Switch) -> Option<RGB8> {
        None
    }
}

// Number of physical switches plus all standard modifiers
const MAX_KEYS: usize = 16 + 8;

pub struct AllKeys {
    keys: heapless::Vec<Keyboard, MAX_KEYS>,
}

impl AllKeys {
    pub fn new<K: Keymap>(keypad: &Keypad, map: K) -> Self {
        let mut consolidated: heapless::Vec<Keyboard, MAX_KEYS> = heapless::Vec::new();
        let pressed_keys = ALL_SWITCHES
            .into_iter()
            .filter(|s| keypad.is_pressed(*s))
            .flat_map(|s| map.keys(s));
        for k in pressed_keys {
            if consolidated.iter().any(|existing| *existing == k) {
                continue;
            }
            if consolidated.push(k).is_err() {
                break;
            }
        }
        Self { keys: consolidated }
    }

    pub fn iter(&self) -> core::slice::Iter<Keyboard> {
        self.keys.iter()
    }
}

impl<'a> IntoIterator for &'a AllKeys {
    type Item = &'a Keyboard;
    type IntoIter = core::slice::Iter<'a, Keyboard>;
    fn into_iter(self) -> Self::IntoIter {
        self.iter()
    }
}
