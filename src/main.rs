#![no_std]
#![no_main]

use core::panic::PanicInfo;

use cortex_m_rt::entry;

use embedded_hal::prelude::*;

use embedded_time::duration::{Milliseconds, Seconds};
use embedded_time::fixed_point::FixedPoint;
use embedded_time::rate::{Extensions, Fraction};
use embedded_time::Instant;

use is31fl3731::devices::Keybow2040 as Display;

use keybow_2048_firmware::keymap::{AllKeys, Keymap};

use rgb::RGB8;

use rp2040_hal::gpio::Pins;
use rp2040_hal::i2c::I2C;
use rp2040_hal::pac::{CorePeripherals, Peripherals};
use rp2040_hal::usb::UsbBus;
use rp2040_hal::{Clock, Sio, Timer, Watchdog};

use usb_device::class_prelude::*;
use usb_device::prelude::*;

use usbd_human_interface_device::device::keyboard::NKROBootKeyboardInterface;
use usbd_human_interface_device::page::Keyboard;
use usbd_human_interface_device::prelude::*;

use keybow_2048_firmware::input::{Switch, ALL_SWITCHES};
use keybow_2048_firmware::make_keypad;

#[link_section = ".boot2"]
#[no_mangle]
#[used]
pub static BOOT2_FIRMWARE: [u8; 256] = rp2040_boot2::BOOT_LOADER_W25Q080;

// "Borrowed" from usbd-human-interface-device: https://github.com/dlkj/usbd-human-interface-device/blob/main/examples/src/lib.rs
pub struct TimerClock<'a> {
    timer: &'a Timer,
}

impl<'a> TimerClock<'a> {
    pub fn new(timer: &'a Timer) -> Self {
        Self { timer }
    }
}

impl<'a> embedded_time::clock::Clock for TimerClock<'a> {
    type T = u32;
    // Fixed-frequency timer; see RP2040 datasheet, section 4.6.1
    const SCALING_FACTOR: Fraction = Fraction::new(1, 1_000_000u32);

    fn try_now(&self) -> Result<Instant<Self>, embedded_time::clock::Error> {
        Ok(Instant::new(self.timer.get_counter_low()))
    }
}

struct MainKeymap;

impl Keymap for MainKeymap {
    type Keys = heapless::Vec<Keyboard, 2>;
    fn keys(&self, switch: Switch) -> Self::Keys {
        let key = |k| heapless::Vec::from_slice(&[k]).unwrap();
        let keys = |k1, k2| heapless::Vec::from_slice(&[k1, k2]).unwrap();
        match switch {
            Switch::Sw3 => key(Keyboard::F5), // Debug: start
            Switch::Sw2 => keys(Keyboard::LeftShift, Keyboard::F5), // Debug: stop
            Switch::Sw7 => key(Keyboard::F10), // Debug: step over
            Switch::Sw6 => key(Keyboard::F6), // Debug: pause
            Switch::Sw11 => key(Keyboard::F11), // Debug: step in
            Switch::Sw10 => keys(Keyboard::LeftShift, Keyboard::F11), // Debug: step out
            _ => heapless::Vec::new(),
        }
    }
    fn color(&self, switch: Switch) -> Option<RGB8> {
        let rgb = |r, g, b| Some(RGB8::new(r, g, b));
        match switch {
            Switch::Sw3 => rgb(0, 255, 0),    // Debug: start
            Switch::Sw2 => rgb(255, 0, 0),    // Debug: stop
            Switch::Sw7 => rgb(0, 255, 255),  // Debug: step over
            Switch::Sw6 => rgb(255, 255, 0),  // Debug: pause
            Switch::Sw11 => rgb(0, 0, 255),   // Debug: step in
            Switch::Sw10 => rgb(255, 0, 255), // Debug: step out
            _ => None,
        }
    }
}

const SLEEP_INTERVAL: Seconds = Seconds(60);

#[entry]
fn main() -> ! {
    use rp2040_hal::clocks;

    let mut peripherals = Peripherals::take().unwrap();
    let core = CorePeripherals::take().unwrap();

    const EXTERNAL_XTAL_FREQ_HZ: u32 = 12_000_000; // Assumed to match the Raspberry Pi Pico
    let mut watchdog = Watchdog::new(peripherals.WATCHDOG);
    let clocks = clocks::init_clocks_and_plls(
        EXTERNAL_XTAL_FREQ_HZ,
        peripherals.XOSC,
        peripherals.CLOCKS,
        peripherals.PLL_SYS,
        peripherals.PLL_USB,
        &mut peripherals.RESETS,
        &mut watchdog,
    )
    .ok()
    .unwrap();
    let timer = Timer::new(peripherals.TIMER, &mut peripherals.RESETS);
    let clock = TimerClock::new(&timer);

    let sio = Sio::new(peripherals.SIO);
    let pins = Pins::new(
        peripherals.IO_BANK0,
        peripherals.PADS_BANK0,
        sio.gpio_bank0,
        &mut peripherals.RESETS,
    );

    let sda_pin = pins.gpio4.into_mode();
    let scl_pin = pins.gpio5.into_mode();
    let i2c = I2C::i2c0(
        peripherals.I2C0,
        sda_pin,
        scl_pin,
        400.kHz(),
        &mut peripherals.RESETS,
        clocks.peripheral_clock,
    );

    let mut delay = cortex_m::delay::Delay::new(core.SYST, clocks.system_clock.freq().integer());
    let mut device = Display::configure(i2c);
    device.setup(&mut delay).unwrap();
    let mut display = Display { device };

    let keypad = make_keypad!(pins);

    let usb_bus = UsbBus::new(
        peripherals.USBCTRL_REGS,
        peripherals.USBCTRL_DPRAM,
        clocks.usb_clock,
        true,
        &mut peripherals.RESETS,
    );
    let usb_alloc = UsbBusAllocator::new(usb_bus);

    let mut keyboard = UsbHidClassBuilder::new()
        .add_interface(NKROBootKeyboardInterface::default_config(&clock))
        .build(&usb_alloc);

    let mut usb_dev = UsbDeviceBuilder::new(&usb_alloc, UsbVidPid(0x16D0, 0x08C6))
        .manufacturer("Pimoroni")
        .product("Keybow 2040")
        .serial_number("TEST")
        .build();

    let mut poll_keys = timer.count_down();
    poll_keys.start(Milliseconds(10));

    let mut usb_tick = timer.count_down();
    usb_tick.start(Milliseconds(1));

    let mut sleep_timer = timer.count_down();
    let mut awake = true;
    sleep_timer.start(SLEEP_INTERVAL);

    let mut color_switch = |sw: Switch, color: RGB8| {
        let (x, y) = sw.coords();
        display.pixel_rgb(x, y, color.r, color.g, color.b).unwrap();
    };

    for sw in ALL_SWITCHES {
        color_switch(sw, MainKeymap.color(sw).unwrap_or(RGB8::new(0, 0, 0)));
    }

    loop {
        let mut do_wake = false;

        if poll_keys.wait().is_ok() {
            if ALL_SWITCHES.into_iter().any(|s| keypad.is_pressed(s)) {
                do_wake = true;
            }
            let keys = AllKeys::new(&keypad, MainKeymap);
            match keyboard.interface().write_report(&keys) {
                Ok(_) => {}
                Err(UsbHidError::Duplicate | UsbHidError::WouldBlock) => {}
                Err(e) => panic!("Error writing keyboard report: {:?}", e),
            };
        }

        if usb_tick.wait().is_ok() {
            match keyboard.interface().tick() {
                Ok(_) | Err(UsbHidError::WouldBlock) => {}
                Err(e) => {
                    panic!("Error processing keyboard tick: {:?}", e)
                }
            };
        }

        if usb_dev.poll(&mut [&mut keyboard]) {
            match keyboard.interface().read_report() {
                Ok(_) => {}
                Err(UsbError::WouldBlock) => {}
                Err(e) => {
                    panic!("Error reading keyboard report: {:?}", e)
                }
            }
        }

        if do_wake {
            if !awake {
                for sw in ALL_SWITCHES {
                    color_switch(sw, MainKeymap.color(sw).unwrap_or(RGB8::new(0, 0, 0)));
                }
            }
            awake = true;
            sleep_timer.start(SLEEP_INTERVAL);
        } else if awake && sleep_timer.wait().is_ok() {
            for sw in ALL_SWITCHES {
                color_switch(sw, RGB8::new(0, 0, 0));
            }
            awake = false;
        }
    }
}

#[panic_handler]
fn on_panic(_: &PanicInfo) -> ! {
    loop {}
}
