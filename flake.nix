{
  description = "Firmware project for the Pimoroni Keybow 2040";
  inputs = {
    dream2nix.url = "github:nix-community/dream2nix";
    elf2uf2-rs = {
      url = "./nix/elf2uf2-rs";
      inputs.dream2nix.follows = "dream2nix";
    };
    nixCargoIntegration = {
      url = "github:yusdacra/nix-cargo-integration";
      inputs.dream2nix.follows = "dream2nix";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.rustOverlay.follows = "rust-overlay";
    };
    nixpkgs.url = "github:nixos/nixpkgs/nixos-22.05";
    rust-overlay = {
      url = "github:oxalica/rust-overlay";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };
  outputs = inputs:
    inputs.nixCargoIntegration.lib.makeOutputs {
      root = ./.;
      overrides = {
        shell = common: prev: {
          packages =
            prev.packages
            ++ [
              common.pkgs.flip-link
              inputs.elf2uf2-rs.packages.${common.system}.elf2uf2-rs
            ];
        };
      };
    };
}
