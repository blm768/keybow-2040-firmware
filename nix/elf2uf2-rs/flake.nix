{
  inputs = {
    dream2nix.url = "github:nix-community/dream2nix";
    src.url = "github:JoNil/elf2uf2-rs";
    src.flake = false;
  };

  outputs = {
    self,
    dream2nix,
    nixpkgs,
    src,
  }: let
    pkgs = nixpkgs.legacyPackages.x86_64-linux;
  in
    (dream2nix.lib.makeFlakeOutputs {
      systems = ["x86_64-linux"];
      config.projectRoot = ./.;
      source = src;
      settings = [
        {
          builder = "crane";
          translator = "cargo-lock";
        }
      ];
      packageOverrides = let
        overrides = {
          add-deps = {
            buildInputs = [pkgs.udev];
            nativeBuildInputs = old: old ++ [pkgs.pkg-config];
          };
        };
      in {
        elf2uf2-rs = overrides;
        elf2uf2-rs-deps = overrides;
      };
    })
    // {
      checks.x86_64-linux.elf2uf2-rs = self.packages.x86_64-linux.elf2uf2-rs;
      defaultPackage.x86_64-linux = self.packages.x86_64-linux.elf2uf2-rs;
    };
}
